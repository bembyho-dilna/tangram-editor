class Tan {
    constructor(options) {
        this._tangram = options.tangram;
        this._tangramActiveElement = options.tangramActiveElement;
        this._element = options.element;
        this._id = options.id;
        this._origin = this._element.points[0];

        this._dragging = false;
        this._translation = {x: 0, y: 0};
        this._currentTranslation = {x: 0, y: 0};


        this._addListeners()
    }

    _addListeners() {
        this._element.addEventListener("mousedown", this._startDragging.bind(this));
        this._element.addEventListener("mousemove", this._drag.bind(this));
        this._element.addEventListener("mouseup", this._endDragging.bind(this));
        this._element.addEventListener("mouseleave", this._endDragging.bind(this));
    }

    _startDragging(event) {
        this._tangramActiveElement.setAttributeNS('http://www.w3.org/1999/xlink', 'href', `#${this._id}`);
        this._element.classList.add(Tan.CssClass.DRAGGING);
        this._dragStartPosition = this._getMousePosition(event);
        this._dragging = true;
    }

    _drag(event) {
        if(this._dragging){
            event.preventDefault();
            const mousePosition = this._getMousePosition(event);
            this._currentTranslation = {
                x: mousePosition.x - this._dragStartPosition.x,
                y: mousePosition.y - this._dragStartPosition.y
            };
            const translateX = this._translation.x + this._currentTranslation.x;
            const translateY = this._translation.y + this._currentTranslation.y;
            this._element.setAttribute('transform',`translate(${translateX},${translateY})`);
        }
    }

    _endDragging(event){
        if(this._dragging){
            this._element.classList.remove(Tan.CssClass.DRAGGING);
            this._translation = {
                x: this._translation.x + this._currentTranslation.x,
                y: this._translation.y + this._currentTranslation.y
            };
            this._dragStartPosition = null;
            this._dragging = false;
        }

    }

    _getMousePosition(event) {
        const CTM = this._tangram.getScreenCTM();
        return {
            x: (event.clientX - CTM.e) / CTM.a,
            y: (event.clientY - CTM.f) / CTM.d
        };
    }
}

Tan.CssClass = {
    DRAGGING: 'dragging'
};

export default Tan;