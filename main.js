import Tan from './Tan.js';

const tangram = document.getElementById('tangram');
const tangramActiveElement = document.getElementById('tangram-active-element');

const tans = [
    'tr-l-1',
    'tr-l-2',
    'tr-m',
    'tr-s-1',
    'sq',
    'rh',
    'tr-s-2'
];

tans.forEach(id => {
    const element = document.getElementById(id);
    return new Tan({
        element,
        tangram,
        tangramActiveElement,
        id
    })
});




